import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: 'counter',
    initialState: {
        value: 0
    },
    reducers: {
        increment: function(currentState, action) {
            console.log("Increment Called: ",action);
            return {...currentState, value: currentState.value + 1};
        },
        decrement: function(currentState, action) {
            console.log("Decrement Called: ",action);
            return {...currentState, value: currentState.value - 1};
        },
    }
});

export const { increment, decrement } = counterSlice.actions;
const counterReducer = counterSlice.reducer;
export default counterReducer;