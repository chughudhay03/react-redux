import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { decrement, increment } from '../store/counterSlice';

export const Counter = () => {
    const value = useSelector(state => state.counter.value);
    const dispatch = useDispatch();

    const handleIncrement = () => {
        dispatch(increment("Hello World from Increment"));
    }

    const handleDecrement = () => {
        dispatch(decrement("Hello World from Decrement"));
    }
  return (
    <div>
        <button onClick={handleIncrement} style={{marginRight:'5px'}}>Add</button>
        {value}
        <button onClick={handleDecrement} style={{marginLeft:'5px'}}>Sub</button>    
    </div>
  )
}
